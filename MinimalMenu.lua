--[[
Copyright (c) 2012 John Glover

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished 
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
]]

require 'lib.middleclass'
require 'Input'
require 'Color'
require 'UpdatableColor'

MinimalMenu = class('MinimalMenu')

local Node = class('Node')

-- Initializes a new Node.
-- @param self The node.
-- @param menu The menu owning the node.
-- @param parent The parent node.
-- @param name Name of the node.
-- @param choose Function executed if the node is activated.
-- @param hint Hint displayed while the node is active.
function Node:initialize(menu, parent, name, choose, hint, defaultChild)
	self.menu = menu
	self.parent = parent
	self.name = name
	self.hint = hint or ''
	self.choose = choose or function( ) menu:activateNode(self) end
	self.children = { }
	self.selectedChild = nil
	self.defaultChild = defaultChild
	self.position = nil
	self.font = nil
	self.color = Color(0, 0, 0, 0)
	self.enabled = true
end

-- Gets the string representation of the node.
-- @param self The node.
-- @return String representation of the node.
function Node:__tostring()
	return self.name .. ' : ' .. self.hint
end

-- Size of the masking gradient on top and bottom of the menu.
local gradientSize = 64

-- Default menu name.
MinimalMenu.static.Name = 'Menu'

-- Default hint used to display when drawing the menu.
MinimalMenu.static.Hint = 
[[
Use the up or down arrow to select, right arrow or [enter] 
to choose and left arrow or [backspace] to go back
]]

-- Default horizontal position for the current selected item is 1/3 of the screen size.
MinimalMenu.static.HorizontalPosition = 1/2

-- Default vertical position for the current selected item is 1/3 of the screen size.
MinimalMenu.static.VerticalPosition = 1/2

-- Default menu item color is white.
MinimalMenu.static.MainColor = Color(255, 255, 255, 255)

-- Default selected menu item color is green.
MinimalMenu.static.SelectedColor = UpdatableColor(Color(0, 128, 0, 255), UpdatableColor.Pulse)

-- Default title color is blue.
MinimalMenu.static.TitleColor = Color(65, 105, 225, 255)

-- Default menu font size.
MinimalMenu.static.MenuFontSize = 24

-- Default title font size.
MinimalMenu.static.TitleFontSize = 36

MinimalMenu.static.SelectedOffsetMultiplier = 1.4

MinimalMenu.static.HintSizeMultiplier = 2/3

MinimalMenu.static.ItemHeightMultiplier = 7/4

MinimalMenu.static.ItemWidthMultiplier = 8

-- Initializes a new MinimalMenu.
-- @param name Name of the menu [Menu]
-- @param hint Menu hint displayed when the menu is drawn.
-- @param titleSize Font size for the title.
-- @param itemSize Font size for menu items.
-- @param wrap Wrap menu when moving up or down past the ends.
function MinimalMenu:initialize(name, hint, titleSize, itemSize, wrap)
	self.name = name or MinimalMenu.Name
	self.hint = hint or MinimalMenu.Hint
	self.head = Node(self, nil, self.name, self.hint)
	self.current = self.head
	self.titleSize = titleSize or MinimalMenu.TitleFontSize
	self.itemSize = itemSize or MinimalMenu.MenuFontSize
	self.titleFont = love.graphics.newFont(self.titleSize)
	self.itemFont = love.graphics.newFont(self.itemSize)
	self.selectedItemFont = love.graphics.newFont(self.itemSize * MinimalMenu.SelectedOffsetMultiplier)
	self.hintFont = love.graphics.newFont(self.itemSize * MinimalMenu.HintSizeMultiplier)
	self.wrap = wrap
	self.itemHeight = self.itemSize * MinimalMenu.ItemHeightMultiplier
	self.itemWidth = self.itemSize * MinimalMenu.ItemWidthMultiplier
	self:setDisplay()
	
	self.input = Input()
	self.input:bindKeystroke('up', function() self:selectPrevious() end, nil)
	self.input:bindKeystroke('w', function() self:selectPrevious() end, nil)
	self.input:bindKeystroke('down', function() self:selectNext() end, nil)
	self.input:bindKeystroke('s', function() self:selectNext() end, nil)
	self.input:bindKeystroke('left', function() self:activateParent() end, nil)
	self.input:bindKeystroke('a', function() self:activateParent() end, nil)
	self.input:bindKeystroke('backspace', function() self:activateParent() end, nil)
	self.input:bindKeystroke('right', function() self:activateSelected() end, nil)
	self.input:bindKeystroke('d', function() self:activateSelected() end, nil)
	self.input:bindKeystroke('return', function() self:activateSelected() end, nil)
end

-- Selects the current node's next child if one exists.
-- @param self The menu.
function MinimalMenu:selectNext()
	local max = table.getn(self.current.children)
	local enabled = false
	
	while not enabled do
		if self.wrap then
			self.current.selectedChild = self.current.selectedChild + 1
			if self.current.selectedChild > max then self.current.selectedChild = 1 end
		else
			self.current.selectedChild = math.min(self.current.selectedChild + 1, max)
		end
		
		enabled = self.current.children[self.current.selectedChild].enabled
	end
end

-- Selects the current node's previous child if one exists.
-- @param self The menu.
function MinimalMenu:selectPrevious()
	local enabled = false
	
	while not enabled do
		if self.wrap then
			local max = table.getn(self.current.children)
			self.current.selectedChild = self.current.selectedChild - 1 
			if self.current.selectedChild < 1 then self.current.selectedChild = max end
		else
			self.current.selectedChild = math.max(self.current.selectedChild - 1, 1)
		end
		
		enabled = self.current.children[self.current.selectedChild].enabled
	end
end

-- Activates the current node's parent.
-- @param self The menu.
function MinimalMenu:activateParent()
	self.current.selectedChild = self.current.defaultChild or self.current.selectedChild
	self.current = self.current.parent or self.head
end

-- Activates the current node's selected child.
-- @param self The menu.
function MinimalMenu:activateSelected()
	self.current.defaultChild = self.current.selectedChild
	self.current.children[self.current.selectedChild].choose()
end

-- Adds an item to the menu.
-- @param self The menu.
-- @param parent The parent node of the new child. [nil]
-- @param name Name of the new node.
-- @param hint Hint to display when the node is active.
-- @param choose Function to execute when the item is selected. [move to child menu]
-- @param default True if this node should be the default child.
-- @return The newly added node.
function MinimalMenu:addItem(parent, name, hint, choose, default)
	parent = parent or self.head
	local newNode = Node(self, parent, name, choose, hint)
	table.insert(parent.children, newNode)
	if default then 
		parent.defaultChild = table.getn(parent.children)
		parent.selectedChild = parent.defaultChild 
	end
	if parent.selectedChild == nil then parent.selectedChild = 1 end
	return newNode
end

-- Activates the node.
-- @param self The menu.
-- @param node The node to activate.
function MinimalMenu:activateNode(node)
	self.current.defaultChild = self.current.selectedChild
	node = node or self.current.children[self.current.selectedChild]
	self.current = node
end

-- Updates the display characteristics for a node and its children, then adds them to the list.
-- @param displayNode Node containing children to update.
-- @param color Color to display the menu items in.
-- @param selectedColor Color to display the selected menu item.
-- @param x The x display origin for the node.
-- @param y The y display origin for the node.
-- @param verticalOffset Offset between menu items.
-- @param outList list to add the displayable menu items to.
-- @return The populated out list.
local function makeDisplayable(displayNode, color, font, selectedColor, selectedFont, x, y, verticalOffset, outList)
	local list = { }

	local currentPosition = 0
	local primaryPosition = nil
	for _, node in ipairs(displayNode.children) do
		if node == displayNode.children[displayNode.selectedChild] then
			primaryPosition = currentPosition
			node.color = selectedColor
			node.font = selectedFont
		else
			node.color = color
			node.font = font
		end
		
		if not node.enabled then
			node.color = color:copy():setColorRgb(nil, nil, nil, node.color.alpha / 4)
		end
		
		node.position = { x = x, y = currentPosition }
		table.insert(list, node)
		currentPosition = currentPosition + 1
	end
	
	-- revisiting each displayable node to calculate the vertical position.
	for _, node in ipairs(list) do
		node.position.y = (node.position.y - primaryPosition) * verticalOffset + y 
		table.insert(outList, node)
	end
	
	return outList
end

-- Updates the menu's displayable nodes.
-- @param self The menu.
function MinimalMenu:update(dt)
	local width = (love.graphics.getWidth() * self.horizontalPosition) - (0.5 * self.itemWidth)
	local height = (love.graphics.getHeight() * self.verticalPosition) - (0.5 * self.itemHeight)
	
	self.selectedColor:update(dt)
	
	local displayableNodes = { }
	
	makeDisplayable(self.current, 
		self.mainColor, 
		self.itemFont,
		self.selectedColor, 
		self.selectedItemFont,
		width,
		height,
		self.itemHeight, 
		displayableNodes)
	
	if self.current.parent ~= nil then
		makeDisplayable(self.current.parent, 
			self.mainColor:copy():setColorRgb(nil, nil, nil, self.mainColor.alpha / 4),
			self.itemFont,
			self.selectedColor.color:copy():setColorRgb(nil, nil, nil, self.selectedColor.color.alpha / 4),
			self.itemFont,
			width - self.itemWidth, 
			height,
			self.itemHeight, 
			displayableNodes)
	end
	
	if self.current.selectedChild ~= nil then
		makeDisplayable(self.current.children[self.current.selectedChild],
			self.mainColor:copy():setColorRgb(nil, nil, nil, self.mainColor.alpha / 4),
			self.itemFont,
			self.selectedColor.color:copy():setColorRgb(nil, nil, nil, self.selectedColor.color.alpha / 4),
			self.itemFont,
			width + self.itemWidth, 
			height,
			self.itemHeight, 
			displayableNodes)
	end
	
	self.displayableNodes = displayableNodes
end

-- Sets the display characteristics for the menu.
-- @param self The menu.
-- @param horizontalPosition Horizontal position as a fraction of the viewport width to center the menu on.
-- @param verticalPosition Vertical position as a fraction of the viewport height to center the menu on.
-- @param mainColor Color used to draw menu items. [r = 255, g = 255, b = 255, a = 255]
-- @param selectedColor Color used to draw the selected item. [r = 0, g = 128, b = 0, a = 255]
function MinimalMenu:setDisplay(horizontalPosition, verticalPosition, mainColor, selectedColor)
	self.horizontalPosition = horizontalPosition or MinimalMenu.HorizontalPosition
	self.verticalPosition = verticalPosition or MinimalMenu.VerticalPosition
	self.mainColor = mainColor or MinimalMenu.MainColor
	self.selectedColor = selectedColor or MinimalMenu.SelectedColor
end

-- Draws the menu.
-- @param self The menu.
function MinimalMenu:draw()
	local width = love.graphics.getWidth()
	local height = love.graphics.getHeight()
	
	local originalColor = Color(love.graphics.getColor())
	local font = love.graphics.getFont() or love.graphics.newFont()
	
	love.graphics.setColor(Color.TranslucentBlack:getColorRgb())
	love.graphics.rectangle('fill', 0, 0, width, height)
	
	for _, item in ipairs(self.displayableNodes) do
		love.graphics.setColor(item.color:getColorRgb())
		love.graphics.setFont(item.font)
		love.graphics.printf(item.name, item.position.x, item.position.y - item.font:getHeight() / 2, self.itemWidth, 'center')
	end

	love.graphics.setColor(Color.Black:getColorRgb())
	local boxHeight = self.titleSize * 1.5
	love.graphics.rectangle('fill', 0, 0, width, boxHeight)
	love.graphics.rectangle('fill', 0, height - boxHeight, width, boxHeight)
	love.graphics.draw(MinimalMenu.Gradient, 0, boxHeight + gradientSize, 0, width, -1)
	love.graphics.draw(MinimalMenu.Gradient, 0, height - boxHeight - gradientSize, 0, width, 1)
	
	love.graphics.setColor(MinimalMenu.TitleColor:getColorRgb())
	
	love.graphics.setFont(self.titleFont)
	love.graphics.printf(self.name, 0, 10, width, 'center')
	
	love.graphics.setFont(self.hintFont)
	love.graphics.printf(self.current.children[self.current.selectedChild].hint or self.hint, 0, height - (2 * self.itemSize), width, 'center')
	
	love.graphics.setFont(font)
	love.graphics.setColor(originalColor:getColorRgb())
end

-- Setting up a gradient used to slowly mask off menu items when they
-- are too far toward the top or bottom of the menu.
local data = love.image.newImageData(1, gradientSize)
for i = 0, gradientSize - 1 do
	data:setPixel(0, i, 0, 0, 0, (i*4))
end
MinimalMenu.static.Gradient = love.graphics.newImage(data)

