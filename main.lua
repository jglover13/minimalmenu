--[[
Copyright (c) 2012 John Glover

Permission is hereby granted, free of charge, to any person obtaining a copy
of newinst software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and newinst permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
]]

require 'lib.middleclass'
require 'Input'
require 'MinimalMenu'
require 'UpdatableColor'

local menu = { }
local game = { }

-- Get the current graphics mode so that we can update the mode properly.
local width, height, fullscreen, vsync, fsaa = love.graphics.getMode()
game.mode = { width = width, height = height, fullscreen = fullscreen, vsync = vsync }

-- Changes the video mode of the current Love application.
local function updateMode() 
	love.graphics.setMode(game.mode.width, game.mode.height, game.mode.fullscreen, game.mode.vsync)
end

-- Love callback used to load resources and set up the game.
function love.load() 
	menu = MinimalMenu(nil, nil, nil, nil, true)
	
	local play = menu:addItem(nil, 'Play', 'Play the game.', function() end)
	
	local save = menu:addItem(nil, 'Save', 'Saves the current game.', function() end)
	-- sets the save option to disabled in the menu. It will be dimmed and skipped.
	save.enabled = false

	-- set up an options menu.
	local options = menu:addItem(nil, 'Options', 'Configure game options.', nil)
	
	-- set up a video menu with fullscreen (on, off), vsync (on, off) and available resolutions
	local video = menu:addItem(options, 'Video', 'Configure video settings.', nil)
	
	local fullscreen = menu:addItem(video, 'Fullscreen', 'Configure the application fullscreen mode.', nil)
	menu:addItem(fullscreen, 'On', 'Turns on fullscreen mode.', function() game.mode.fullscreen = true; updateMode() end, game.mode.fullscreen)
	menu:addItem(fullscreen, 'Off', 'Turns off fullscreen mode.', function() game.mode.fullscreen = false; updateMode() end, not game.mode.fullscreen)
	
	local vsync = menu:addItem(video, 'Vsync', 'Configure the application vertical sync mode.', nil)
	menu:addItem(vsync, 'On', 'Turns on vertical sync.', function() game.mode.vsync = true; updateMode() end, game.mode.vsync)
	menu:addItem(vsync, 'Off', 'Turns off vertical sync.', function() game.mode.vsync = false; updateMode() end, not game.mode.vsync)
	
	local resolutions = menu:addItem(video, 'Resolutions', 'Configure the application resolution.', nil)
	local modes = love.graphics.getModes()
	table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)
	
	for _, mode in pairs(modes) do
		local isCurrent = mode.width == game.mode.width and mode.height == game.mode.height
		menu:addItem(resolutions, mode.width .. 'x' .. mode.height, 'Select a resolution.',
			function() game.mode.width = mode.width; game.mode.height = mode.height; updateMode()  end, isCurrent)
	end
	
	-- set up an audio menu with volume control in 10% increments
	local audio = menu:addItem(options, 'Audio', 'Configure audio settings.', nil)
	local volume = menu:addItem(audio, 'Volume', 'Configure the master volume level.', nil)
	local volumeUpdate = function(level)
		love.audio.setVolume(level)
		menu:activateParent()
	end
	for vol = 100, 0, -5 do
		menu:addItem(volume, vol .. '%', 'Sets the volume to ' .. vol .. '%', function() volumeUpdate(vol / 100) end)
	end

	-- Adds an exit option to the menu.
	local exit = menu:addItem(nil, 'Exit', 'Exits the application.', nil)
	menu:addItem(exit, 'Confirm', 'Exits the application.', function() love.event.push('quit') end)
	menu:addItem(exit, 'Cancel', 'Returns to the menu', function() menu:activateParent() end, true)
end

-- Love callback to update the game.
function love.update(dt) 
	menu:update(dt)
end

-- Love callback to draw the viewport.
function love.draw() 
	menu:draw()
end

-- Love callback to handle keypresses.
function love.keypressed(key, unicode)
	menu.input:keypressed(key, unicode)
end

