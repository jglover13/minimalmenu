--[[
Copyright (c) 2012 John Glover

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished 
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
]]

require 'lib.middleclass'

Input = class('Input')

-- Creates a new Input system.
function Input:initialize()
	self.keyBindings = { }
	self.mouseBindings = { }
	self.mouseOn = true
	self.joystickBindings = { }
	self.joystickOn = false
end

-- Binds a keystroke to a 'command'.
-- @param self Input system to bind the keystroke to.
-- @param key Key to bind.
-- @param onDown Command to bind the key down event to.
-- @param onUp Command to bind the key up event to.
-- @return True if the keystroke was successfully bound; false otherwise.
function Input:bindKeystroke(key, onDown, onUp, ...)
	assert(onDown == nil or type(onDown) == 'function', 'Parameter [onDown] must be a function type.')
	assert(onUp == nil or type(onUp) == 'function', 'Parameter [onUp] must be a function type.')
	
	if self.keyBindings[key] == nil then
		self.keyBindings[key] = { onDown = onDown, onUp = onUp, active = love.keyboard.isDown(key), arg = arg }
		return true
	--else
		-- this is interesting and clever, but not really useful; I'm just loathe to delete it
		--self.keyBindings[key].onDown = function (...) self.keyBindings[key](unpack(arg))  onDown(unpack(arg)) end
		--self.keyBindings[key].onUp = function (...) self.keyBindings[key](unpack(arg))  onDown(unpack(arg)) end
	end
	
	return false
end

-- Binds a mouse button to a 'command'.
-- @param self Input system to bind the mouse button to.
-- @param button Button to bind.
-- @param onDown Command to bind the mouse button down event to.
-- @param onUp Command to bind the mouse button up event to.
-- @return True if the mouse button was successfully bound; false otherwise.
function Input:bindMouse(button, onDown, onUp, ...)
	assert(onDown == nil or type(onDown) == 'function', 'Parameter [onDown] must be a function type.')
	assert(onUp == nil or type(onUp) == 'function', 'Parameter [onUp] must be a function type.')
	
	if self.mouseBindings[button] == nil then
		self.mouseBindings[button] = { onDown = onDown, onUp = onUp, active = love.mouse.isDown(button), arg = arg }
		return true
	end
	
	return false
end

-- Binds a joystick button to a 'command'.
-- @param self Input system to bind the joystick button to.
-- @param button Button to bind.
-- @param onDown Command to bind the joystick button down event to.
-- @param onUp Command to bind the joystick button up event to.
-- @return True if the joystick button was successfully bound; false otherwise.
function Input:bindJoystick(joystick, button, onDown, onUp)
	assert(onDown == nil or type(onDown) == 'function', 'Parameter [onDown] must be a function type.')
	assert(onUp == nil or type(onUp) == 'function', 'Parameter [onUp] must be a function type.')
	
	if self.joystickBindings[joystick .. ',' .. button] == nil then
		self.joystickBindings[joystick .. ',' .. button] = { onDown = onDown, onUp = onUp, active = love.joystick.isDown(joystick, button) }
		return true
	end
	
	return false
end

-- Unbinds a keystroke.
-- @param self Input system.
-- @param key Key to unbind.
function Input:unbindKeystroke(key)
	self.keyBindings[key] = nil
end

-- Unbinds a keystroke.
-- @param self Input system.
-- @param button Button to unbind.
function Input:unbindKeystroke(button)
	self.mouseBindings[button] = nil
end

-- Unbinds a keystroke.
-- @param self Input system.
-- @param button Button to unbind.
function Input:unbindKeystroke(button)
	self.joystickBindings[button] = nil
end

-- Toggles the mouse state.
-- @param self Input system which should have the mouse toggled.
-- @param on Whether the mouse should be enabled.
function Input:toggleMouse(on)
	self.mouseOn = on
end

-- Toggles the state of all joysticks or a particular joystick.
-- @param self Input system.
-- @param on Whether the joystick should be enabled.
-- @param joystick Identifier of the joystick to toggle.
function Input:toggleJoystick(on, joystick)
	self.joystickOn = on
	
	if joystick and on and love.joystick.getNumJoysticks >= joystick then
		love.joystick.open(joystick)
	elseif joystick and not on then
		love.joystick.close(joystick)
	end
end

function Input:mousepressed(x, y, button)
	if self.mouseBindings[button] and self.mouseBindings[button].onDown and self.mouseOn then
		self.mouseBindings[button].active = true
		self.mouseBindings[button].onDown(unpack(self.mouseBindings[button].arg), x, y, button)
	end
end

function Input:mousereleased(x, y, button)
	if self.mouseBindings[button] and self.mouseBindings[button].onUp and self.mouseOn then
		self.mouseBindings[button].active = true
		self.mouseBindings[button].onUp(unpack(self.mouseBindings[button].arg), x, y, button)
	end
end

function Input:keypressed(key, unicode)
	if self.keyBindings[key] and self.keyBindings[key].onDown then
		self.keyBindings[key].active = true
		self.keyBindings[key].onDown(unpack(self.keyBindings[key].arg), key)
	end
end

function Input:keyreleased(key, unicode)
	if self.keyBindings[key] and self.keyBindings[key].onUp then
		self.keyBindings[key].active = false
		self.keyBindings[key].onUp(unpack(self.keyBindings[key].arg), key)
	end
end

-- TODO: clean up the joystick inputs to check for index before trying to access function
function Input:joystickpressed(joystick, button)
	if self.joystickBindings[joystick .. ',' .. button].onDown then
		self.joystickBindings[joystick .. ',' .. button].active = true
		self.joystickBindings[joystick .. ',' .. button].onDown(joystick, button)
	end
end

function Input:joystickreleased(joystick, button)
	if self.joystickBindings[joystick .. ',' .. button].onUp then
		self.joystickBindings[joystick .. ',' .. button].active = false
		self.joystickBindings[joystick .. ',' .. button].onUp(joystick, button)
	end
end