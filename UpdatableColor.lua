--[[
Copyright (c) 2012 John Glover

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished 
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
]]

require 'lib.middleclass'
require 'Color'

UpdatableColor = class('UpdatableColor', Color)

-- Initializes a new color instance.
-- @param self The color
-- @param color The original color.
-- @param translate The function of dt which will give the current color.
function UpdatableColor:initialize(color, translate)
	self.color = color or Color.White
	self.translate = translate or UpdatableColor.Pulse
	Color.initialize(self, self.color.red, self.color.green, self.color.blue, self.color.alpha)
end

-- Update the current display characteristics of the color.
-- @param self The updatable color.
-- @param dt Delta time since the last update.
function UpdatableColor:update(dt)
	self.red, self.green, self.blue, self.alpha = self.translate(dt, self.color)
end

-- Predefined function to make the color pulse.
-- @param dt Delta time since the last update.
-- @param color The original color.
-- @return The red, green, blue and alpha values of the color at the given time.
UpdatableColor.static.Pulse = 
	function(dt, color)
		local brighten = (math.cos(love.timer.getTime() * 1.8 * math.pi) + 1) / 2 * 60
		return 
			math.max(color.red - brighten, 0), 
			math.max(color.green - brighten, 0), 
			math.max(color.blue - brighten, 0), 
			color.alpha
	end

-- Predefined function to make the color blink.
-- @param dt Delta time since the last update.
-- @param color The original color.
-- @return The red, green, blue and alpha values of the color at the given time.
UpdatableColor.static.Blink = 
	function(dt, color)
		return color.red, color.green, color.blue, (math.cos(love.timer.getTime() * math.pi) + 1) / 2 * 255
	end