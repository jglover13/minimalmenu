--[[
Copyright (c) 2012 John Glover

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished 
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
]]

require 'lib.middleclass'

Color = class('Color')

-- Initializes a new color instance.
-- @param self The color.
-- @param red The red value.
-- @param green The green value.
-- @param blue The blue value.
-- @param alpha The alpha value.
function Color:initialize(red, green, blue, alpha)
	self.red = red or 0
	self.green = green or 0
	self.blue = blue or 0
	self.alpha = alpha or 0
end

-- Copies the color.
-- @param self The color.
-- @return Copy of the original color.
function Color:copy()
	return Color(self.red, self.green, self.blue, self.alpha)
end

-- Gets the color as RGB values.
-- @param self The color.
-- @return Red, green, blue and alpha of the color.
function Color:getColorRgb()
	return self.red, self.green, self.blue, self.alpha
end

-- Gets the color as HSL values
-- @param self The color.
-- @return Hue, saturation, lightness and alpha of the color.
function Color:getColorHsl()
	local red, green, blue = self.red / 255, self.green / 255, self.blue / 255
	local max, min = math.max(r, math.max(g, b)), math.min(r, math.min(g, b))
	local hue, saturation, lightness = (max + min) / 2
	
	if max == min then 
		hue = 0 
		saturation = 0 
	else
		local d = max - min
		if lightness > 0.5 then saturation = d / (2 - max - min)
		else saturation = d / (max + min) end
		
		if max == red then 
			hue = (green - blue) / d
			if green < blue then hue = hue + 6 end
		elseif max == green then
			hue = (blue - red) / d + 2
		elseif max == blue then
			hue = (red - green) / d + 4
		end
		
		hue = hue / 6
	end
	
	return hue, saturation, lightness, self.alpha
end

-- Sets the color using RGB values.
-- @param self The color.
-- @param red The red value.
-- @param green The green value.
-- @param blue The blue value.
-- @param alpha The alpha value.
-- @return The color.
function Color:setColorRgb(red, green, blue, alpha)
	self.red = red or self.red
	self.green = green or self.green
	self.blue = blue or self.blue
	self.alpha = alpha or self.alpha
	return self
end

-- Sets the color using HSL values.
-- @param self The color.
-- @param hue The hue value.
-- @param saturation The saturation value.
-- @param lightness The lightness value.
-- @param alpha The alpha value.
-- @return The color.
function Color:setColorHsl(hue, saturation, lightness, alpha)
	if saturation<=0 then self:setColorRgb(lightness, lightness, lightness, alpha) end
		
	hue, saturation, lightness = hue / 256*6, saturation / 255, lightness / 255
	local c = (1 - math.abs(2 * lightness - 1)) * saturation
	local x = (1 - math.abs(hue % 2 - 1)) * c
	local m, red, green, blue = (lightness - 0.5 * c), 0, 0, 0
		
	if h < 1 then red, green, blue = c, x, 0
	elseif hue < 2 then red, green, blue = x, c, 0
	elseif hue < 3 then red, green, blue = 0, c, x
	elseif hue < 4 then red, green, blue = 0, x, c
	elseif hue < 5 then red, green, blue = x, 0, c
	else red, green, blue = c, 0, x end 
		
	self:setColorRgb((red + m) * 255, (green + m) * 255, (blue + m) * 255, alpha)
	
	return self
end

function Color:__tostring()
	return '{ r = ' .. self.red .. ', g = ' .. self.green .. ', b = ' .. self.blue .. ', a = ' .. self.alpha .. ' }'
end

-- Fades this color to a second color.
-- @param self The color.
-- @param time Zero-based time to evaluate the color.
-- @param prologue How many seconds the resulting color will be equal to the original color.
-- @param attack How many seconds to transition from fully opaque original color to transparent original color.
-- @param sustain How many seconds the resulting color will be fully transparent.
-- @param decay How many seconds to transition from fully transparent end color to fully opaque end color.
-- @param epilogue How many seconds the result color will be equal to the fade out color.
-- @param color The fade-out color.
-- @return Resulting color based on the inputs.
function Color:fadeToColor(time, prologue, attack, sustain, decay, epilogue, color)
  -- [0, prologue)
  if time < prologue then
    return Color(self.red, self.green, self.blue, 255)
  end
   
  -- (prologue, prologue + attack]
  time = time - prologue
  if time < attack then
    return Color(self.red, self.green, self.blue, ( math.cos( time / attack * math.pi ) + 1 ) / 2 * 255)
  end
   
  -- (prologue + attack, prologue + attack + sustain]
  time = time - attack
  if time < sustain then
    return Color(self.red, self.green, self.blue, 0)
  end
   
  -- (prologue + attack + sustain, prologue + attack + sustain + decay]
  time = time - sustain
  if time < decay then
    return Color(color.red, color.green, color.blue, 255 - ( ( math.cos( time / decay * math.pi ) + 1 ) / 2 * 255 ))
  end
   
  -- (prologue + attack + sustain + decay, prologue + attack + sustain + decay + epilogue]
  time = time - decay
  if time < epilogue then
    return Color(color.red, color.green, color.blue, 255)
  end
end

-- The color black.
Color.static.Black = Color(0, 0, 0, 255)

-- Black with half the alpha value.
Color.static.TranslucentBlack = Color(0, 0, 0, 128)

-- The color white.
Color.static.White = Color(255, 255, 255, 255)
